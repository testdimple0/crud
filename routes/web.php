<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/users', 'UsersController@index')->name('users');
Route::get('/users/fetch_data', 'UsersController@fetch_data');
Route::post('/users/update_data', 'UsersController@update_data')->name('users.update_data');
Route::post('/user/add', 'UsersController@store')->name('user.add');
Route::post('/user/delete_data', 'UsersController@delete_data')->name('user.delete_data');
Route::post('/user/bulk_delete', 'UsersController@bulk_delete')->name('user.bulk_delete');
Route::post('/user/file_upload', 'UsersController@file_upload')->name('user.file_upload');