<!DOCTYPE html>
<html>
 <head>
  <title>Users</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
    .float-right{
        float:right
    }
    #user_form{
        display:none;
    }
    .avatar_img{
        cursor:pointer;
    }
    .fileinput{
        display:none;
    }
    </style>
   
 </head>
 <body>
  <br />
  <div class="container box">
   <h3 align="center">Practical Test</h3><br />

   <div class="panel panel-default" id="users_table">
    <div class="panel-heading">Users <span class="float-right"> <button type="button" class="btn btn-primary btn-xs add_new"> Add new </button>  |    <button type="button" class="btn btn-danger btn-xs bulk_delete"> Bulk Delete </button></span></div>
    <div class="panel-body">
     <div id="message"></div>
     <div class="table-responsive">
      <table class="table table-striped table-bordered">
       <thead>
        <tr>
         <th>Sr No</th>
         <th>Select</th>
         <th>Name</th>
         <th>Contact no</th>
         <th>Hobby</th>
         <th>Category</th>
         <th>Profile Pic</th>
         <th>Action</th>
        </tr>
       </thead>
       <tbody>       
       </tbody>
      </table>
      {{ csrf_field() }}
     </div>
    </div>
   </div>

   <div class="panel panel-default" id="user_form" >
    <div class="panel-heading">Add User<span class="float-right user_list"> <button class="btn btn-xs btn-primary" type="button">  Users List </button> </span></div>
    <div class="panel-body">
        <div id="message_form"></div>
        <div class="row" style="padding-left:15px;">     
            <form action="/user/add" method="post" id="userFormId" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="title">Name</label>
                    </div>
                    <div class="col-md-7">
                        <input type="text" class="form-control" id="name" required name="name">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="title">Contact No</label>
                    </div>
                    <div class="col-md-7">
                        <input type="text" class="form-control" id="contact" required  name="contact">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="title">Hobby</label>
                    </div>
                    <div class="col-md-7">
                        <div class="row checkbox-group required">
                            <div class="col-md-3">
                                <input type="checkbox" class="" name="hobby[]" value="Programming">Programming
                            </div>
                            <div class="col-md-3">
                                <input type="checkbox" class="" name="hobby[]" value="Games">Games 
                            </div>
                            <div class="col-md-3">
                                <input type="checkbox" class="" name="hobby[]" value="Reading">Reading 
                            </div>
                            <div class="col-md-3">
                                <input type="checkbox" class="" name="hobby[]" value="Photography"> Photography
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="description">Category</label>
                    </div>
                    <div class="col-md-7">
                        <select class="form-control" name="category_id" required>
                        @foreach(json_decode($category) as $key => $value)
                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                        @endforeach
                        </select>
                    </div>                    
                </div>
                <div class="row form-group">
                    <div class="col-md-3">
                        <label for="description">Profile Pic</label>
                    </div>
                    <div class="col-md-7">
                        <input type="file" id="avatar" name="avatar" accept="image/*">
                    </div>                    
                </div>

                <button type="submit" class="btn btn-sm btn-success">Save</button>
                <button type="button" class="btn btn-sm btn-danger">Cancel</button>
            </form>
        </div>
    </div>
   </div>
  </div>
 </body>
</html>

<script>
$(document).ready(function(){
    $('#users_table').css('display','block');
    $('#user_form').css('display','none');

    fetch_data();

    function fetch_data()
    {
        $.ajax({
        url:"/users/fetch_data",
        dataType:"json",
        success:function(data)
        {
            var html = '';  
            if(data.length > 0){
                for(var count=0; count < data.length; count++)
                {   
                    if(data[count].avatar.trim() != ""){
                        var img_path = '{{ URL::to("/") }}/uploads/'+ data[count].avatar;
                    }else{
                        var img_path = '{{ URL::to("/") }}/uploads/no-image.png';
                    }
                    
                    html +='<tr>';
                    html +='<td>'+ (count+1) +'</td>';
                    html +='<td><input type="checkbox" class="del_checkbox" data-id="'+data[count].id+'" name="delete_ids[]" value="'+data[count].id+'"/></td>';
                    html +='<td contenteditable class="column_name" data-column_name="name" data-id="'+data[count].id+'">'+data[count].name+'</td>';
                    html += '<td contenteditable class="column_name" data-column_name="contact" data-id="'+data[count].id+'">'+data[count].contact+'</td>';
                    html += '<td contenteditable class="column_name" data-column_name="hobby" data-id="'+data[count].id+'">'+data[count].hobby+'</td>';
                    html += '<td contenteditable class="category_column" data-column_name="category_id" data-id="'+data[count].id+'">'+data[count].category_name+'</td>';

                    html += '<td data-column_name="avatar" data-fileid="'+data[count].id+'"><img src="'+img_path+'" class="avatar_img" width="50" height="50" name="avatar"/><input name="upload" type="file" data-id="'+data[count].id+'" class="fileinput" style="display:none;" /></td>';
                
                    html += '<td><button type="button" class="btn btn-danger btn-xs delete" id="'+data[count].id+'">Delete</button></td></tr>';
                }
            } else{
                html += '<h4>No record found</h4>';
            }       
            
            $('tbody').html(html);
        }
        });
    }

    var _token = $('input[name="_token"]').val();

    $(document).on('blur', '.column_name', function(){
        var column_name = $(this).data("column_name");
        var column_value = $(this).text();
        var id = $(this).data("id");
        
        if(column_value.trim() != '')
        {
        $.ajax({
            url:"{{ route('users.update_data') }}",
            method:"POST",
            data:{column_name:column_name, column_value:column_value, id:id, _token:_token},
            success:function(data)
            {
            $('#message').html(data);
            }
        })
        }
        else
        {
        $('#message').html("<div class='alert alert-danger'>Enter some value</div>");
        }
    });


    $(document).on('click','.add_new', function(){
        $('#users_table').css('display','none');
        $('#user_form').css('display','block');
    })

    $(document).on('click','.user_list', function(){
        $('#users_table').css('display','block');
        $('#user_form').css('display','none');
    })

    $('#userFormId').on('submit',function(event){
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        if($('div.checkbox-group.required :checkbox:checked').length > 0){
            $.ajax({
                url: "/user/add",
                type:"POST",
                data:formData,
                processData: false,
                contentType: false,
                success:function(response){           
                    fetch_data();         
                    $('#users_table').css('display','block');
                    $('#message').html("<div class='alert alert-success'>User add successfully.</div>");
                    $('#user_form').css('display','none');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                  if (jqXHR.status == 500) {
                    $('#message_form').html('Internal error: ' + jqXHR.responseText);
                  } else {
                    $('#message_form').html('Unexpected error.');
                  }
              }
            });
        }else{
            $('#message_form').html("<div class='alert alert-danger'>PLease select at least one hobby.</div>");
        }     
    })

    $(document).on('click', '.delete', function(){
        var id = $(this).attr("id");
        if(confirm("Are you sure you want to delete this record?"))
        {
            $.ajax({
                url:"{{ route('user.delete_data') }}",
                method:"POST",
                data:{id:id, _token:_token},
                success:function(data)
                {
                    $('#message').html(data);
                    fetch_data();
                }
            });
        }
    });

    $(document).on('click','.bulk_delete', function(e){
        e.preventDefault();

        var checkedNum = $('input[name="delete_ids[]"]:checked').length;
        if (!checkedNum) {
            $('#message').html("<div class='alert alert-danger'>Please select at least one user.</div>");
        }else{
            if(confirm("Are you sure you want to delete this records?"))
            {
                var allVals = [];  
                $("input[name='delete_ids[]']:checked").each(function() {  
                    allVals.push($(this).attr('data-id'));
                });

                var join_selected_values = allVals.join(","); 

                $.ajax({
                    url: "{{ route('user.bulk_delete') }}",
                    method:"POST",
                    data:{ids:join_selected_values, _token:_token},
                    success: function (data) {
                        fetch_data();         
                        //$('#users_table').css('display','block');
                        $('#message').html(data);
                        fetch_data();
                        //$('#user_form').css('display','none');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status == 500) {
                            $('#message').html('Internal error: ' + jqXHR.responseText);
                        } else {
                            $('#message').html('Unexpected error.');
                        }
                    }
                });


            }
        }        
    })

    $(document).on('click','.avatar_img', function(e){
     $(this).next('.fileinput').trigger('click');    
    })
    
    $(document).on('change','.fileinput', function(){
       
        var file_data = $(this).prop('files')[0];   
        var form_data = new FormData(); 
        form_data.append('id', $(this).attr("data-id"));                 
        form_data.append('file', file_data);
        form_data.append('_token', _token);
        $.ajax({
            url: "{{ route('user.file_upload') }}",
            type: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
                fetch_data();
            }
        });
    });

    $(document).on('click', '.category_column', function(){
        var id = $(this).attr("data-id");
        var html = "";
        var data = "{{ $category }}";
        var category = JSON.parse(data.replace(/&quot;/g,'"'));
        html+= "<select name='category' class='form-control cat_select' data-id='"+id+"'>";
        category.forEach(element => {
            html+= '<option value="'+element.id+'">'+element.name+'</option>';            
        });
        html += "</select>";
        $(this).html(html)
       
    })

    $(document).on('click', '.category_column', function(){
    });

    $(document).on('change', 'select.cat_select', function(){  
        var column_name = 'category_id';
        var column_value = this.value;
        var id =  $(this).attr("data-id");
        
        if(column_value.trim() != '')
        {
        $.ajax({
            url:"{{ route('users.update_data') }}",
            method:"POST",
            data:{column_name:column_name, column_value:column_value, id:id, _token:_token},
            success:function(data)
            {
                fetch_data();
                $('#message').html(data);
                
            }
        })
        }
        else
        {
        $('#message').html("<div class='alert alert-danger'>Enter some value</div>");
        }
    });

});
</script>
