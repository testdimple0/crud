<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Category;

class UsersController extends Controller
{
    public function index(){
        $data = Category::get();
        $category = json_encode($data);
       
       return view('users.index',compact('category'));
    }

    public function fetch_data(Request $request){
        $data =  User::select('users.*','categories.name as category_name')->join('categories','categories.id','=','users.category_id')->orderBy('users.id','desc')->get();
        echo json_encode($data);
    }

    public function update_data(Request $request)
    {
        if($request->ajax())
        {
            $data = array(
                $request->column_name       =>  $request->column_value
            );
            User::where('id', $request->id)
                ->update($data);
            echo '<div class="alert alert-success">Data Updated</div>';
        }
    }

    public function store(Request $request){
        if($request->hasFile('avatar')) {         
            $fileName = time().'.'.$request->avatar->extension();  
            $request->avatar->move(public_path('uploads'), $fileName);
        }else{
            $fileName = "";
        }

        $request = [
            'name' => $request->name,
            'contact' => $request->contact,
            'category_id'=> $request->category_id,
            'hobby'=> implode(',',$request->hobby),
            'avatar'=>$fileName
        ];

        $data =User::insertGetId($request);
        echo json_encode($data);
    }

    public function delete_data(Request $request)
    {
        if($request->ajax())
        {
            User::where('id', $request->id)->delete();
            echo '<div class="alert alert-success">User Deleted</div>';
        }
    }

    public function bulk_delete(Request $request)
    {
        if($request->ajax())
        {
            $ids = $request->ids;
            User::whereIn('id',explode(",",$ids))->delete();
            echo '<div class="alert alert-success">Users Deleted</div>';
        }
    }

    public function file_upload(Request $request){
        if($request->hasFile('file')) {  
            $fileName = time().'.'.$request->file->extension();  
            $request->file->move(public_path('uploads'), $fileName);
            $data['avatar'] = $fileName;
            User::where('id', $request->id)
                ->update($data);
            echo json_encode($data);
        }
    }
}
